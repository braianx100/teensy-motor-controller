
#ifndef _INT_PARAMETER_H
#define _INT_PARAMETER_H

#include <Arduino.h>
#include <stdint.h>

//--------------------------------------------------------------------------------------------------
// class IntParameter
//
// This class wraps an integer. It is meant to mimic similar usage in Java where an integer is
// wrapped in an object so it may be modified by a method as Java methods cannot directly modify
// parameters and can return only one result.
//
// Although this class is not necessary in C/C++, it is used to make this code as similar as
// possible to Java code.
//

class IntParameter{

	public:

		IntParameter() { i = 0; }

		IntParameter(int pValue){ i = pValue; }

		void init(){}

		virtual ~IntParameter();

	protected:

	private:

	// class member variables

	public:

		int i;

	protected:

	public:


};// end of class IntParameter
//--------------------------------------------------------------------------------------------------

#endif // _INT_PARAMETER_H
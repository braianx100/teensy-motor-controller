//-----------------------------------------------------------------------------------------
// SpudMainPiBackpack.ino
//
// For all Skoonie projects, the INO file does NOTHING except create the MainController object,
// initialize it, and call its process() function repeatedly.
//
// HOWEVER...for Unit Testing projects the INO file instead creates the UnitTest object,
// initializes it, and calls its process() function repeatedly. The UnitTest object should
// create any objects that need to be tested.
//
//
// See notes at the top of the MainController.cpp file for details pertaining to each project.
//

#include "MainController.h"

MainController mainController;

//-----------------------------------------------------------------------------------------
// setup
//
// This is the standard Arduino "setup" function which is called once on startup.
//

void setup() {

  mainController.init();
  
}// end of setup
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// loop
//
// This is the standard Arduino "loop" function which loops continuously after "setup"
// function has been executed.
//
  
void loop(void) { 

  mainController.process();

}// end of loop
//-----------------------------------------------------------------------------------------

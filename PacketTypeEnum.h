
#ifndef _PACKET_TYPE_ENUM_H
#define _PACKET_TYPE_ENUM_H

//-----------------------------------------------------------------------------------------
// PacketTypeEnum enumeration
//
// Defines various types of packets.
//

enum class PacketTypeEnum{
	NO_PKT = 0,
	ACK_PKT = 1,
	GET_DEVICE_INFO = 2,
	LOG_MESSAGE = 3,
	STOP_NECK_TURN = 4,
	START_RANDOM_NECK_TURNS = 5,
	STOP_RANDOM_NECK_TURNS = 6,
	START_FULL_NECK_TURNS = 7,
	STOP_FULL_NECK_TURNS = 8,
	SET_NECK_MOTOR_DRIVE_CURRENT_LEVEL = 9,
	TURN_NECK_SPECIFIED_STEPS = 10
};

//-----------------------------------------------------------------------------------------

#endif // _PACKET_TYPE_ENUM_H

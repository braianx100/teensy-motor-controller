//--------------------------------------------------------------------------------------------------
// HostEthernetLinkHandler.cpp
//
// This class handles Ethernet communication between the Control board and the Host device.
//
// NOTE
//
//	Function checkForPacketReady should be called often to prevent serial buffer overflow.
//
//----------------------------------------------------
//
// Authors:
//
//	Mike Schoonover		10/26/2020
//	Hunter Schoonover
//
//
//--------------------------------------------------------------------------------------------------

#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-variable"


#include "HostEthernetLinkHandler.h"

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
// class HostEthernetLinkHandler
//
/**
 * This class handles serial communication between the Control board and the Host Computer.
 *
 */


//--------------------------------------------------------------------------------------------------
// HostEthernetLinkHandler::HostEthernetLinkHandler (constructor)
//
/**
 *
 * @param pAddress	address for this device on the network
 *
 */

HostEthernetLinkHandler::HostEthernetLinkHandler(const uint8_t pThisDeviceID,
				EthernetLink *pEthernetLink, PacketTool *const pPacketTool) :
				thisDeviceID(pThisDeviceID), ethernetLink(pEthernetLink), packetTool(pPacketTool)
{


}// end of HostEthernetLinkHandler::HostEthernetLinkHandler (constructor)
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// HostEthernetLinkHandler::init
//
/**
 * Initializes the object. Should be called after instantiation.
 *
 */

void HostEthernetLinkHandler::init()
{

	setupIO();

}// end of HostEthernetLinkHandler::init
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// HostEthernetLinkHandler::doRunTimeTasks
//
/**
 * Handles runtime tasks..
 *
 * This function should be called often during run time to allow for continuous processing.
 *
 */

int HostEthernetLinkHandler::doRunTimeTasks()
{

	int status;

	status = handleEthernetConnection();

	return(status);

}// end of HostEthernetLinkHandler::doRunTimeTasks
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// HostEthernetLinkHandler::handleEthernetConnection
//
/**
 *
 * Handles communications and handlers interactions with remote program with this device acting as
 * a Host.
 *
 * This function should be called often during run time to allow for continuous processing.
 *
 * If the DHCP information has not been retrieved from the server, this function will attempt to
 * do so. After the DHCP info has been retrieved, Ethernet.maintain() will be called with each
 * pass to allow the renewal of the DHCP lease as necessary.
 *
 * As a Host, if the remote program is not currently connected then this function will check for
 * connection requests and accept the first one to arrive.
 *
 * If the DHCP info has not been retrieved or a client has not connected, this function returns
 * without doing anything.
 *
 * If a connection is already active, this function will monitor the incoming data stream for
 * packets and process them.
 *
 * An EthernetLink object only handles one remote client device at a time. To handle more hosts or
 * clients, a new EthernetLink object should be created for each.
 *
 *
 */

int HostEthernetLinkHandler::handleEthernetConnection()
{

	int status = 0;

	if(!ethernetLink->getIsDHCPConfigured()){
		status = ethernetLink->configureDHCPByRouter();
		if(status != 1)	{ return(0); }
	} else{
		Ethernet.maintain(); //call constantly to allow renewal of DHCP lease
	}

	if(!ethernetLink->getIsConnected()){
		status = ethernetLink->connectToRemote();
		if(status != 1)	{ return(0); }
		packetTool->setStreams(ethernetLink->getStream(), ethernetLink->getStream());
	}

	return(0); //debug mks

	//return( handleCommunications() ); debug mks remove this

}// end of HostEthernetLinkHandler::handleEthernetConnection
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// HostEthernetLinkHandler::handleCommunications
//
/**
 * Handles communications with the remote device. This function should be called often during
 * runtime to allow for continuous processing.
 *
 * This function will monitor the incoming data stream for packets and process them as they are
 * received.
 *
 */

int HostEthernetLinkHandler::handleCommunications() //debug mks remove this function
{

	ethernetLink->doRunTimeTasks();

	bool packetReady = packetTool->checkForPacketReady();

	if (packetReady){ handlePacket(); }

	return(0);

}// end of HostEthernetLinkHandler::handleCommunications
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// HostEthernetLinkHandler::handlePacket
//
/**
 *
 * Handles a packet received from the remote device.
 *
 * returns 0 on no packet handled, 1 on packet handled, -1 on error
 *
 */

int HostEthernetLinkHandler::handlePacket()
{

	return(0);

}// end of HostEthernetLinkHandler::handlePacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// HostEthernetLinkHandler::sendACKPkt
//
/**
 * Sends an ACK packet to the host with status code pStatus.
 *
 * @param pSourcePktType	the type of packet being acknowledged.
 * @param pStatus			the PacketStatusEnum value to be sent to host as data byte in packet
 *
 */

void HostEthernetLinkHandler::sendACKPkt(PacketTypeEnum pSourcePktType, PacketStatusEnum pStatus)
{

	packetTool->sendBytes(PacketTypeEnum::ACK_PKT, 2, pSourcePktType, pStatus);

}// end of HostEthernetLinkHandler::sendACKPkt
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// HostEthernetLinkHandler::setupIO
//
/**
 * Sets up generic Digital and Analog Input/Output pins. Pins for dedicated functions such
 * as SPI, U2C, or PWM are set up in other functions.
 *
 */

void HostEthernetLinkHandler::setupIO()
{

}// end HostEthernetLinkHandler::setupIO
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// HostEthernetLinkHandler::HostEthernetLinkHandler (destructor)
//

HostEthernetLinkHandler::~HostEthernetLinkHandler()
{

}// end of HostEthernetLinkHandler::HostEthernetLinkHandler (destructor)
//--------------------------------------------------------------------------------------------------

//end of class HostEthernetLinkHandler
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------


#ifndef _MAIN_CONTROLLER_H
#define _MAIN_CONTROLLER_H

#include <Arduino.h>

#include "Tools.h"
#include "PacketTool.h"
#include "HostEthernetLinkHandler.h"
#include "MotorControllerHandlerPWM.h"

//--------------------------------------------------------------------------------------------------
// class MainController
//
// holds information that is shared between Objects
//

class MainController{

	public:

		MainController();

		void init();

		void process();

		virtual ~MainController();


	protected:

		void setupIO();

		void initSerialPort();

		void handlePacket();

		void handleErrorFlags(const ErrorFlag pErrorFlag);

		void displayLoopExecutionTime(const int pNumLoopsRun);

		void debug();

	public:

		static const uint8_t HOST_ID = 0;

		static const uint8_t THIS_DEVICE_ID_ON_NETWORK = 2;

		static const int THIS_DEVICE_PORT = 4244;

		static const int SERIAL_TIMEOUT_MILLIS = 1000;

	protected:

		HostEthernetLinkHandler *hostCom;

		MotorControllerHandlerPWM *motorHandler;

		uint32_t loopCount = 0, startTime = millis();


};// end of class MainController
//--------------------------------------------------------------------------------------------------

#endif // _MAIN_CONTROLLER_H

#ifndef _MOTOR__CONTROLLER_HANDLER_PWM_H
#define _MOTOR__CONTROLLER_HANDLER_PWM_H

#include <Arduino.h>

#include "ErrorFlags.h"
#include "Tools.h"

//-----------------------------------------------------------------------------------------
// class MotorControllerHandlerPWM
//
// This class controls the motor controller(s).
//

class MotorControllerHandlerPWM{

	public:

		MotorControllerHandlerPWM();

		void init();

		int doRunTimeTasks();

		virtual ~MotorControllerHandlerPWM();

	protected:

		void setupIO();

	private:

	// class member variables

	public:

	protected:

	private:

};// end of class MotorControllerHandlerPWM
//-----------------------------------------------------------------------------------------

#endif // _MOTOR__CONTROLLER_HANDLER_PWM_H
/******************************************************************************
* Title: CommunicationsInterface.h
* Author: Mike Schoonover
* Date: 03/13/23
*
* Purpose:
*
* This file contains the interface definition for CommunicationsInterface.  This interface provides
* basic read/write/query functions applicable to a communications port such as a Stream, serial
* port, etc.
*
* All functions are declared as virtual so this class serves as an interface.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------


#ifndef _COMMUNICATIONS_INTERFACE_H
#define _COMMUNICATIONS_INTERFACE_H

#include <Arduino.h>
#include <string>

#include "IntParameter.h"
#include "LoggerBasicInterface.h"
#include "InetAddress.h"

//-----------------------------------------------------------------------------------------
// class CommunicationsInterface
//
// (all Functions virtual for use as an Interface)
//
// This file contains the interface definition for CommunicationsInterface.  This interface provides
// basic read/write/query functions applicable to a communications port such as a Stream, serial
// port, etc.
//
// All functions are declared as virtual so this class serves as an interface.
//

class CommunicationsInterface{


	public:

		CommunicationsInterface(){}

		virtual void init(LoggerBasicInterface *pThreadSafeLogger);

		virtual int configureDHCPByRouter();

		virtual int connectToRemote(std::string pSelectedCOMPort, int pBaudRate);

		virtual int connectToRemote(IPAddress pIPAddr, int pPort);

		virtual int doRunTimeTasks();

		virtual bool closeConnectionToRemote();

		virtual void open();

		virtual void close();

		virtual bool getIsConnected();

		virtual bool getIsDHCPConfigured();

		virtual int clearReceiveBuffer();

		virtual bool setDTR();

		virtual bool clearDTR();

		virtual int waitForData(long pNumBytesToWaitFor, long pTimeOut,
																IntParameter pNumBytesAvailable);

		virtual int sendBytes(char* pBytes, int pNumBytes);

		virtual char readByte();

		virtual char* readBytesResponse(int pMaxNumBytes);

		virtual int readBytesResponse(long pMaxNumBytes, char* pByteArray,
																		IntParameter pNumBytesRead);

		virtual char peek();

		virtual char* readBytes(int pNumBytes);

		virtual void disposeOfAllResources();

		virtual ~CommunicationsInterface(){}

	protected:

	private:

	// class member variables

	public:

	protected:

	public:


};//end of class CommunicationsInterface
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#endif // _COMMUNICATIONS_INTERFACE_H
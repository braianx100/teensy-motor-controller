
#ifndef _PACKET_TOOL_H
#define _PACKET_TOOL_H

#include <Arduino.h>

#include "PacketTypeEnum.h"
#include "PacketStatusEnum.h"
#include "ErrorFlags.h"
#include "Tools.h"

//-----------------------------------------------------------------------------------------
// class PacketTool
//
// This class handles formatting, reading, and writing of packets.
//

class PacketTool{

	public:

		PacketTool();

		void init(const uint8_t pHostAddress, const uint8_t pThisDeviceAddress);

		void setStreams(Stream * const pInputStream, Stream * const pOutputStream);

		bool checkForPacketReady();

		PacketTypeEnum getPktType(){ return(pktType); }

		int getNumPktDataBytes(){ return(numPktDataBytes); }

		uint8_t getSourceDeviceAddrFromReceivedPkt(){ return (sourceDeviceAddrFromReceivedPkt); }

		uint8_t* getPktDataBuffer() { return (inBuffer); }

		void sendCString(const PacketTypeEnum pPacketType, const char * const pString);

		void sendBytes(const PacketTypeEnum pPacketType, int pNumBytes, ...);

		PacketStatusEnum parseDuplexIntegerFromPacket(int *pIndex, int16_t *pValue);

		int getResyncCount(){ return(resyncCount); }

		bool getHeaderValid(){ return(headerValid); }

		virtual ~PacketTool();

	protected:

		void checkForPacketHeaderAvailable();

		void resync();

		int prepareHeader(uint8_t * const pOutBuffer, const uint8_t pDestAddress,
									const PacketTypeEnum pPacketType, const uint8_t pNumDataBytes);

		int waitForNumberOfBytes(const int pNumBytes, const unsigned long pTimeOutMillis);

		int readBytes(const int pNumBytes);

	public:


	protected:

		uint8_t hostAddress;

		uint8_t thisDeviceAddr;

		int numDataBytesRead;

		int resyncCount;

		bool headerValid;

		PacketTypeEnum pktType;

		Stream *byteIn;

		Stream *byteOut;

		uint16_t numPktDataBytes;

		int numDataBytesPlusChecksumByte;

		int pktChecksum;

		uint8_t destDeviceAddrFromReceivedPkt;

		uint8_t sourceDeviceAddrFromReceivedPkt;

		static const int OUT_BUFFER_SIZE = 128;
		static const int IN_BUFFER_SIZE = 128;

		uint8_t outBuffer[OUT_BUFFER_SIZE];

		uint8_t inBuffer[IN_BUFFER_SIZE];

};// end of class PacketTool
//-----------------------------------------------------------------------------------------

#endif // _PACKET_TOOL_H
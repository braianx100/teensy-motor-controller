//-----------------------------------------------------------------------------------------
// Tools.cpp
//
//
//

#include "Tools.h"

//-----------------------------------------------------------------------------------------
// printDoubleToSerialPort
//
// Prints a floating point double value to the serial com port.
//
// Arduino's compilers do not have support for printing floating point values due to the
// size of the library.
//
// Prints pVal with number of decimal places specified by pPrecision.
// NOTE: precision is 1 followed by the number of zeros for the desired number of decimial places
// example: printDouble( 3.1415, 100); // prints 3.14 (two decimal places)
//
// NOTE: not necessary for ARM processers such as on the Adafruit Grand Central; to enable
// printf/sprintf for floats, first execute one time:
//
//		asm(".global _printf_float");
//

void printDoubleToSerialPort( const double pVal, unsigned int pPrecision){

   Serial.print (int(pVal));  //prints the int part
   Serial.print("."); // print the decimal point
   unsigned int frac;
   if(pVal >= 0)
     frac = (pVal - int(pVal)) * pPrecision;
   else
      frac = (int(pVal)- pVal ) * pPrecision;
   int frac1 = frac;
   while( frac1 /= 10 )
       pPrecision /= 10;
   pPrecision /= 10;
   while(  pPrecision /= 10)
       Serial.print("0");

   Serial.println(frac,DEC) ;


}//end of printDoubleToSerialPort
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// decToBCD
//
// Converts normal decimal numbers to binary coded decimal.
//

byte decToBcd(const byte val) {

  return ( (val / 10 * 16) + (val % 10) );

}// end of decToBCD
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// getSineWaveValue
//
// Returns the sine value for pDegrees of magnitude pMagnitude.
// The returned value has a small amount of random variance to simulate a bit of noise.
// The magnitude of the variance is specified by pNoiseLevel.
//

int16_t getSineWaveValue(const int pDegrees, const int pMagnitude, const int8_t pNoiseLevel){

  return(sin(pDegrees * 0.01745329251994329576923690768489) * pMagnitude + random(pNoiseLevel));

}// end of getSineWaveValue
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// dtostrf
//
// converts a double or float to a string
//

char *dtostrf (const double val, const signed char width, const unsigned char prec,
																			char * const sout) {

  char fmt[20];
  sprintf(fmt, "%%%d.%df", width, prec);
  sprintf(sout, fmt, val);

  return sout;

}// end of dtostrf
//-----------------------------------------------------------------------------------------

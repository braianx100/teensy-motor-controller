//--------------------------------------------------------------------------------------------------
// MainController.cpp
//
//
// Main code for the Teensy Motor Controller.
//
// In the Arduino IDE, choose settings (must be done every time you switch projects!!!):
//
//	in "Tools" on Main Menu
//
//		Select Board: Tools/Board/Teensy/Teensy 4.1
//    Board: "Teensy 4.1"
//    Optimize: "Faster"
//    CPU Speed: "600 MHz"
//    USB Type: "Serial"
//		Programmer: ArduinoISP
//
//
//  Board Files Required:
//
//
//  Library Files Required:
//
//
//	Library file of future interest:
//
// Running on other processor boards:
//
//----------------------------------------------------
//
// Authors:
//    Braian Pita
//    Mark
//    Hunter Schoonover
//    Mike Schoonover
//
//--------------------------------------------------------------------------------------------------

#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-variable"

#include "MainController.h"

#define DEBUG_VERBOSE_CONTROLLER 1	// displays extra debugging messages on serial port

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
// class MainController
//
// This class is the main MainController (as in MVC structure).
//

//--------------------------------------------------------------------------------------------------
// MainController::MainController (constructor)
//

MainController::MainController()
{

}// end of MainController::MainController (constructor)
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::init
//
/*
 * top-level init that handles basic initialization and calls initSelf for specific init
 * Must be called after instantiation to complete setup.
 *
 */

void MainController::init()
{

	setupIO();

	initSerialPort();

	Serial.setTimeout(SERIAL_TIMEOUT_MILLIS); //native USB serial port to host (and programming)

	Serial.println("Wheel Controller started...");

	EthernetServer *server = new EthernetServer(THIS_DEVICE_PORT);

	EthernetLink *ethernetLink;
	ethernetLink = new EthernetLink(server, THIS_DEVICE_PORT);
	ethernetLink->init(nullptr);

	PacketTool *packetTool;
	packetTool = new PacketTool();
	packetTool->init(HOST_ID, THIS_DEVICE_ID_ON_NETWORK);

//	Serial.print("stream pointer at init: "); //debug mks
//	Serial.println((uint32_t)ethernetLink->getStream()); //debug mks


	hostCom	= new HostEthernetLinkHandler(THIS_DEVICE_ID_ON_NETWORK, ethernetLink, packetTool),
	hostCom->init();

	motorHandler = new MotorControllerHandlerPWM();
	motorHandler->init();

//	flashLED(6, 300); //debug mks

//	hostCom->echoTest();

//	hostCom->waitForNumberOfBytes(3, 60000);

//	hostCom->sendBytes(7,72,101,108,108,111,10,13); //debug mks "HelloCRLF"

//	Serial.println("test 1"); //debug mks

//	hostCom->sendBytes(HostSerialComHandler::PacketTypeEnum::LOG_MESSAGE, 4, 1, 2, 3, 4); //debug mks

// debug mks ~ for reference
//    sendBytes(HEAD_TRANSLATE_CMD,
//             (byte) ((pDistance >> 8) & 0xff), (byte) (pDistance & 0xff));

}// end of MainController::init
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::process
//
/**
 * This method is called every iteration in the main loop in order to perform processes.
 *
 * Note that the !Serial() check does not always (ever?) detect that the host has closed the
 * connection. This is usually not a problem as the host can reconnect without re-initializing the
 * serial port on this side.
 *
 */

void MainController::process()
{

	if(!Serial){ initSerialPort(); }

	hostCom->doRunTimeTasks();

	delay(1000); //debug mks


	Serial.println("DEBUG 1"); //debug mks

	if(hostCom->getEthernetLink()->getIsConnected()){


//	Serial.print("available after connect: "); //debug mks
//	Serial.println((uint32_t)hostCom->getEthernetLink()->available()); //debug mks


//	Serial.print("stream pointer after connect: "); //debug mks
//	Serial.println((uint32_t)hostCom->getEthernetLink()); //debug mks

		Serial.println("checking for new packet..."); //debug mks

		bool packetReady = hostCom->getPacketTool()->checkForPacketReady();

		//debug mks
		if(packetReady) {
			Serial.print("packet received! ");
			Serial.print("Packet type: ");
			Serial.println((int)hostCom->getPacketTool()->getPktType());
		} //debug mks end

		if(packetReady) { handlePacket(); }

	}

	//debug mks if(packetReady){ handlePacket(); }

}// end of MainController::process
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::handlePacket
//
/**
 * Processes the packet which has been received in the HostSerialComHandler object.
 *
 */

void MainController::handlePacket()
{

	PacketTypeEnum pktType = hostCom->getPacketTool()->getPktType();

//	PacketStatusEnum status;

	switch(pktType){

		case PacketTypeEnum::GET_DEVICE_INFO :
			hostCom->getPacketTool()->sendCString(PacketTypeEnum::LOG_MESSAGE,
															"Hello from the Motor Controller!");
			return;
		break;

		case PacketTypeEnum::LOG_MESSAGE :
			//in the future may display message on a small screen
			hostCom->sendACKPkt(pktType, PacketStatusEnum::PACKET_VALID);
			return;
		break;

		default:

		break;

	}

}// end of MainController::handlePacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::setupIO
//
// Sets up generic Digital and Analog Input/Output pins. Pins for dedicated functions such
// as SPI, U2C, or PWM are set up in other functions.
//

void MainController::setupIO()
{

	// initialize digital pin LED_BUILTIN as an output.
	pinMode(LED_BUILTIN, OUTPUT);

	digitalWrite(LED_BUILTIN, LOW);

}// end MainController::setupIO
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::initSerialPort
//
/**
 * Initializes the port named Serial. This is typically the USB-to-serial port.
 *
 * When using the boards without M4 chips, this function waited until the Serial object
 * became active. When the Ardunio Serial Monitor Panel was not open, this caused a
 * lockup. This also caused a lockup in standalone mode (not connected to Arduino IDE).
 *
 * On some non-M4 boards, a 1.5 second delay after Serial.begin is required or the
 * first serial transmissions will be lost.
 *
 * The original Ardunio code comment stated that the wait is only necessary for a
 * "native USB port".
 *
 */

void MainController::initSerialPort()
{

	Serial.begin(9600);

	flashLED(2, 300);

	while(!Serial);

}// end of MainController::initSerialPort
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::displayLoopExecutionTime
//
// This function calculates the time it has taken to run pNumLoopsRun number of execution
// loops.
//
// Global startTime should have been started before the first loop was run.
//
// Time Analysis at Various SPI speeds:
//
//  Tools->Max SPI: "24 MHz (standard)"
//
//  4  Mhz SPI -> .22 seconds per loop (text works)
//  6  Mhz SPI -> .20 seconds per loop (text works)
//  8  Mhz SPI -> .18 seconds per loop (text fails)
//  12 Mhz SPI -> .17 seconds per loop (text fails)
//  24 Mhz SPI -> .15 seconds per loop (text fails)
//
// NOTHING for SPI speeds up the BMP image display -- QSPI speed issue?
// Tried setting to CPU/2 in Tools, but no improvement.
//

void MainController::displayLoopExecutionTime(const int pNumLoopsRun) {

    int timePassed = (millis() - startTime);

    double timePerLoop = timePassed / pNumLoopsRun;

//debug mks -- can't use printf if not ARM processor    Serial.printf("Time required for %d loops: %d ms", pNumLoopsRun, timePassed);
    //printDoubleToSerialPort(timePassedSecs , 100);
    //Serial.printf("Milliseconds per loop: ");
    //printDoubleToSerialPort(timePerLoop , 1000);
    //Serial.println("");

}// end MainController::displayLoopExecutionTime
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// debug
//

void MainController::debug() {

	return;

	//printBarToSerial('-', 80);
	//Serial.println("-- Debug Section\n");

}// end debug
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::handleErrorFlags
//
// Handles errors based on error codes, printing the proper error messages to serial output.
// todo: the error messages should later be written to an error log file.
//

void MainController::handleErrorFlags(const ErrorFlag pErrorFlag){

/*

	if(pErrorFlag == NO_ERROR) return;

	Warning warning(0, "Warning", "", "Warning0", &shared, 150, 200, 500, 80, RA8875_WHITE,
															LIGHT_GRAY_16BPP, &display, nullptr);
	std::string errorMsg = "ERROR ";

	Serial.print("Error Code: "); Serial.print(pErrorFlag);

	switch(pErrorFlag){
	case ERROR_MALLOC:
		Serial.println("MALLOC ERROR");
		exit(ERROR_MALLOC);   // todo: handle malloc error without exiting
		break;
	case ERROR_ADDINGCHILD:
		Serial.println("ERROR ADDING CHILD TO CHILD ARRAY");
		exit(ERROR_ADDINGCHILD);
		break;
	case ERROR_ADDINGPAGE:
		Serial.println("ERROR ADDING PAGE TO PAGE ARRAY");
		exit(ERROR_ADDINGPAGE);
		break;
	case ERROR_INIT_SPIFLASH:
		Serial.println(" INITIALIZING SPI FLASH MEMORY");
		exit(ERROR_INIT_SPIFLASH);
		break;
	case ERROR_CORRUPT_INI_FILE:
		Serial.println((errorMsg += "CORRUPT INI FILE").c_str());
		warning.show(errorMsg.c_str(), -1);
		break;
	default:
		Serial.println("UNKNOWN EXCEPTION CODE");
		break;
	}

 *
 */

}// end of MainController::handleErrorFlags
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::MainController (destructor)
//

MainController::~MainController()
{

	delete(hostCom);

}// end of MainController::MainController (destructor)
//--------------------------------------------------------------------------------------------------

//end of class MainController
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

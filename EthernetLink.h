
#ifndef _ETHERNET_LINK_H
#define _ETHERNET_LINK_H

#include <Arduino.h>
#include <string>
#include <NativeEthernet.h>

#include "Tools.h"
#include "CommunicationsInterface.h"
#include "PacketTool.h"
#include "InetAddress.h"
#include "MotorControllerHandlerPWM.h"

//--------------------------------------------------------------------------------------------------
// class EthernetLink
//
//

class EthernetLink : public CommunicationsInterface{

	public:

		EthernetLink(EthernetServer *pServer, int pThisDevicePort);

		void init(LoggerBasicInterface *pThreadSafeLogger);

		Stream* getStream();

		virtual int configureDHCPByRouter();

		virtual int connectToRemote(std::string pSelectedCOMPort, int pBaudRate);

		virtual int connectToRemote(IPAddress pIPAddr, int pPort){ return(0); }

		virtual int connectToRemote();

		virtual int doRunTimeTasks();

		virtual bool closeConnectionToRemote();

		virtual void open();

		virtual void close() {}

		virtual bool getIsConnected();

		virtual bool getIsDHCPConfigured();

		virtual int clearReceiveBuffer();

		virtual void sendString(std::string pString);

		virtual std::string readStringResponse();

		virtual std::string readStringResponse(int pMaxNumBytes);

		virtual bool setDTR(){ return(false); }

		virtual bool clearDTR(){ return(false); }

		virtual int waitForData(long pNumBytesToWaitFor, long pTimeOut,
				IntParameter pNumBytesAvailable){ return(0); }

		virtual int sendBytes(char* pBytes, int pNumBytes){ return(0); }

		virtual char readByte(){ return(0); }

		virtual char* readBytesResponse(int pMaxNumBytes);

		virtual int readBytesResponse(long pMaxNumBytes, char* pByteArray,
												IntParameter pNumBytesRead){ return(0); }

		virtual char peek(){ return(0); }

		virtual char* readBytes(int pNumBytes){ return(nullptr); }

		virtual void disposeOfAllResources();

		virtual ~EthernetLink();


	protected:

		EthernetClient* getInputStream();

		EthernetClient* getOutputStream();

	private:

		bool threadSleep(int pSleepTime);

		void logSevere(std::string pMessage);

		std::string convertBytesToCommaDelimitedDecimalString(char *pBuffer);


	// class member variables

	public:

	protected:

		int thisDevicePort;

		bool isDHCPConfigured;

		EthernetServer *server;

		EthernetClient *client;

		EthernetClient clientObject;

		bool isConnected;

		IPAddress ipAddr;

		int numBytesRead;

		std::string inString;

		bool simulate = false;

		LoggerBasicInterface *tsLog;

		std::string ipAddrS;

		EthernetClient socket;

//		BufferedReader in = null;
		char inBuffer[1024];
		char outBuffer[1024];
//		DataOutputStream byteOut = null;
//		DataInputStream byteIn = null;


	private:


};// end of class EthernetLink
//--------------------------------------------------------------------------------------------------

#endif // _ETHERNET_LINK_H